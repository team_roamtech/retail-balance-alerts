from email.message import EmailMessage
from email.utils import formataddr
import smtplib
from config import MAIL_FROM_ADDRESS,MAIL_HOST,MAIL_PORT,MAIL_USERNAME,MAIL_PASSWORD,MAIL_ENCRYPTION
from config import MAIL_TO_SUPPORT_NAME,MAIL_RECIPIENTS_ADDRESSES,MAIL_CC_ADDRESSES

def fill_template(message):
    data = None
    ''' Reads the html template and replaces the message place holder with the actual message'''
    with open('email_template.html', 'r') as file:
        data = file.read().replace('\n', '').replace('MESSAGE_PLACEHOLDER',message)
    return data

def send_email(message,amount):
    # construct email
    email = EmailMessage()

    email['Subject'] = f'PDSL Float: {amount:,}'

    email['From'] = formataddr((MAIL_TO_SUPPORT_NAME, MAIL_FROM_ADDRESS))
    email['To'] = MAIL_RECIPIENTS_ADDRESSES
    email['Cc'] = MAIL_CC_ADDRESSES

    msg = fill_template(message)
    if msg:
        email.set_content(msg, subtype='html')
    else:
        email.set_content(message, subtype='text')

    # Send the message via local SMTP server.
    with smtplib.SMTP(MAIL_HOST, MAIL_PORT) as s:
        s.login(MAIL_USERNAME, MAIL_PASSWORD)
        s.send_message(email)
