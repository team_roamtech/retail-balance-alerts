## Description

This library will read a log file and search for a float balance tag. If found it will send an email alert to the configured mailing list.

## Set up

### Virtual Env
Create python3 virtual env with the required dependancies
```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
### Configuration
Change the default values in config.py

log file
```python
LOG_FILE = /path/to/my/log/file
```
email addresses
```python
MAIL_RECIPIENTS_ADDRESSES = ['email1@mycompany.com','email2@mycompany.com']
MAIL_CC_ADDRESSES = ['email@mycompany.com',]
```
## Usage
```bash
python processor.py 
```
## Automation
Use cron to set up periodic alerts

## Support

* martin.kabachia@roamtech.com
