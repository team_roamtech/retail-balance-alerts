import requests
import json
from config import ACCESS_TOKEN_URL,CLIENT_ID,CLIENT_SECRET,SEND_SMS_URL,SMS_ALERT_USERS

def send_sms(message):
    '''Calls Emalify Send SMS API'''
    #Refresh API key
    response = requests.post(url=ACCESS_TOKEN_URL,json={"client_id":CLIENT_ID,"client_secret":CLIENT_SECRET,"grant_type":"client_credentials",})
    print(response.request.body)
    if response.status_code == 200:
        data = json.loads(response.text.encode('utf8'))
        access_token = data['access_token']
        if access_token:
            #Send the sms
            headers = {"Authorization": f"Bearer {access_token}"}
            payload = {"to": SMS_ALERT_USERS,"message": message,}
            response = requests.post(url=SEND_SMS_URL, headers=headers, json=payload)
            print(response.text.encode('utf8'))
    else:
        print(response.text.encode('utf8'), response.status_code)
