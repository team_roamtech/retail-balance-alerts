#!/usr/bin/env python3
""" The master alerts script 
    Reads the last balance entry in pdsl log and sends it via email
"""
import re

from config import LOG_FILE
from send_email import send_email
from send_sms import send_sms

if __name__ == '__main__':
    retail_balances = {}
    #Read the retail balances in the log file
    with open(LOG_FILE) as f:
        last_valid_time = ""
        for line in f:
            time  = re.search('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}',line)
            if time:
                last_valid_time = time.group(0)
            balance = re.search('retailBalance="(.*?)"',line)
            if balance:
                amount = balance.group(1)
                retail_balances[last_valid_time] = amount
    print(f"Today's Balances: {retail_balances}")

    #Get the latest amount in retail balances, this is the value we will email
    if retail_balances:
        ordered_dates = sorted(retail_balances, reverse=True)
        latest_date = ordered_dates[0]
        print(f"Ordered Dates: {ordered_dates}")
        try:
            float_amount = round(int(retail_balances[latest_date])/100)#Get value at latest date value and convert from shillings to cents
            send_email(f"PDSL float on: <b> {latest_date} </b> was <b> {float_amount:,} </b>",float_amount)
            send_sms(f"PDSL float: {float_amount:,}")
        except ValueError as ex:
            print(f"ex")


